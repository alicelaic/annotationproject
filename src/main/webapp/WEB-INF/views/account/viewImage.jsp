<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>" />
<script src="<c:url value='/resources/js/jquery.blockUI.js'/>"></script>

<form method="POST" action="searchImage">
	<div id="formSearchDiv">
		<input type="text" id="lireValue" name="lireValue"
			placeholder="Place your URL here..." class="form-control" /> <input
			type="submit" id="imageToFoundBtn" value="Proceed"
			class="btn btn-success" />
	</div>
</form>
<div id="request">
	<script>
		$.unblockUI;
	</script>

</div>
<div id="imageInShown">
	<div id="imageDivShn">
		<h2>Image added by you:</h2>
		<a> <img id="imageImgInShown" class="img-responsive"
			src="${imageURL}">
		</a>
	</div>
	<div id="tagsDiv">
		<c:choose>
			<c:when test="${fn:length(tags) gt 0}">
				<h3>
					<a>Tags associated with this image: </a>
				</h3>
				<c:forEach items="${tags}" var="tag">
					<span><strong>${tag.tag.name}</strong></span>

				</c:forEach>
			</c:when>
			<c:otherwise>
				<h3>
					<a>There are no tags associated with this image!</a>
				</h3>
			</c:otherwise>
		</c:choose>
	</div>

</div>
<h2>Image similar with the one above:</h2>
<c:forEach var="i" begin="0" end="${fn:length(images)}">
	<c:choose>
		<c:when test="i%3 = 0">
			<div class="row">
		</c:when>
		<c:when test="i%3 = 2">
			</div>
		</c:when>
	</c:choose>

	<div class="col-md-4 portfolio-item">
		<c:set var="tagsName" value="" />
		<c:forEach items="${images[i].tagsSelected}" var="tag">
			<c:set var="tagsName" value="${tag.tag.name},, ${tagsName }" />
		</c:forEach>

		<a> <img data-id="${tagsName }" class="img-responsive"
			id="${images[i].id }" src="${images[i].link }">
		</a>
		<h3>
			<a>Tags associated with this image: </a>
		</h3>

		<p>
			<c:forEach items="${images[i].tagsSelected}" var="tag">
				<span>${tag.tag.name}</span>
			</c:forEach>
		</p>
	</div>

</c:forEach>

<script>
	$("#imageToFoundBtn").click(function() {
		$.blockUI({
			message : 'Please wait...'
		});
	});
</script>
