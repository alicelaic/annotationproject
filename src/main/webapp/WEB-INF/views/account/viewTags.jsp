<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

 <div class="col-md-4 portfolio-item"> 
	<div class="row">
	 <form>
			<div class="list-group" id="types">
				 <a href="#" class="list-group-item active" id="bus">Bus station</a>
				 <a href="#" class="list-group-item" id="restaurant">Restaurants</a>
				 <a href="#" class="list-group-item" id="school">Education</a>
				 <a href="#" class="list-group-item" id="health">Health</a>
			</div>
	</form>
	</div>
 </div>
  <div class="col-md-4 portfolio-item">

	<div class="row">
		<!--  <img class="img-responsive" id="${image.id }" src="${image.link }"> style="height:500px;weight:500px;"-->
		<div class="img-responsive" id="map"
			style="height:300px;weight:500px;"></div>
	</div>

 </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmBvBrwlQgoPTnmxR0NStI_yNV6xYc5xU&libraries=places&callback=initMap"
	async defer></script>
<script>
	// This example requires the Places library. Include the libraries=places
	// parameter when you first load the API. For example:
	// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
    $('.list-group').on('click', '.list-group-item', function (event) {
 		 event.preventDefault();
 		 console.log(this.id);
 		 $(this).addClass('active').siblings().removeClass('active');
 		deleteMarkers();
 		var service = new google.maps.places.PlacesService(map);
		service.nearbySearch({
			location : pyrmont,
			radius : 500,
			type : [ this.id ]
		}, callback);
    });
    
	var map;
	var infowindow;
	var markers=[];
	
	var pyrmont = {
			lat : 47.1504671,
			lng : 27.5866319
		};

	function initMap() {
		
		map = new google.maps.Map(document.getElementById('map'), {
			center : pyrmont,
			zoom : 15
		});

		infowindow = new google.maps.InfoWindow();
		var service = new google.maps.places.PlacesService(map);
		service.nearbySearch({
			location : pyrmont,
			radius : 500,
			type : [ 'health' ]
		}, callback);
		
		 google.maps.event.addDomListener(document.getElementById('types'),
		            'change', function() {
		              updateMap();
		        });
		      
	}

	function callback(results, status) {
		if (status === google.maps.places.PlacesServiceStatus.OK) {
			for (var i = 0; i < results.length; i++) {
				createMarker(results[i]);
			}
		}
	}
	
	function updateMap()
	{
		alert('update');
	}
	
	function createMarker(place) {
		var placeLoc = place.geometry.location;
		//console.log(place);
		
		 var image = {
				    url: 'http://maps.google.com/mapfiles/ms/icons/pharmacy-us.png',
				    // This marker is 20 pixels wide by 32 pixels high.
				    size: new google.maps.Size(30, 32),
				    // The origin for this image is (0, 0).
				    origin: new google.maps.Point(0, 0),
				    // The anchor for this image is the base of the flagpole at (0, 32).
				    anchor: new google.maps.Point(0, 32)
				  };
		
		var marker = new google.maps.Marker({
			map : map,
			position : place.geometry.location,
			title: place.name
			/*icon: image /*'https://maps.google.com/mapfiles/kml/shapes/library_maps.png'*/
		});
		
		 markers.push(marker);
		 marker.setMap(map);

		/*google.maps.event.addListener(marker, 'click', function() {
			infowindow.setContent(place.name);
			infowindow.open(map, this);
		});*/
	}
	
	function setMapOnAll(map) {
		  for (var i = 0; i < markers.length; i++) {
		    markers[i].setMap(map);
		  }
		}
	function clearMarkers() {
		  setMapOnAll(null);
		}

		// Shows any markers currently in the array.
		function showMarkers() {
		  setMapOnAll(map);
		}

		// Deletes all markers in the array by removing references to them.
		function deleteMarkers() {
		  clearMarkers();
		  markers = [];
		}
</script>