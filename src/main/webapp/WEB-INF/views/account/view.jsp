<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>" />
<script src="<c:url value='/resources/js/jquery.blockUI.js'/>"></script>

<!--  <div style="width: 50%; margin-right: auto; text-align: center; display: inline-flex; margin-left: 50%; margin-bottom: 10px;">
    <form method="get" action="${pageContext.request.contextPath}/image/previous">
        <input type="submit" id="previousBtn"
               value="<" class="btn btn-success"/>
    </form>

    <form method="get" action="${pageContext.request.contextPath}/image/next">
        <input type="submit" id="nextBtn"
               value=">" class="btn btn-success" style="margin-left: 5px;"/>
    </form>
</div>-->
<div id="request">
	<script>
		$.unblockUI;
	</script>
</div>
<c:forEach var="i" begin="0" end="${fn:length(images)}">
    <c:choose>
        <c:when test="i%3 = 0">
            <div class="row">
        </c:when>
        <c:when test="i%3 = 2">
            </div>
        </c:when>
    </c:choose>

    <div style="border: 1px solid grey;" class="col-md-4 portfolio-item" id-image="${images[i].id }">
  
  <p>${images[i].description}</p>
  </br>
  <a href="../realestate/id=${images[i].id }">${images[i].title}</a>
    </div>

</c:forEach>


<script>
	$("#imageToFoundBtn").click(function() {
		$.blockUI({
			message : 'Please wait...'
		});
	});

    $("input[class='delete-button']").click(function(){
        var id = $(this).attr('id-image');

        $.ajax({
            type: 'POST',
            url: 'deleteImage',
            data: {
                imageId: id
            },
            success: function(data){
                //call another ajax to remove
                $("div[id-image='"+id+"']").remove();
            }
        })
    });
</script>
