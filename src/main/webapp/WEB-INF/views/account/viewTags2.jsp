<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-md-2 portfolio-item"></div>
<div class="col-md-4 portfolio-item">

    <h2>${image.name}</h2>

    <div class="row">
        <img class="img-responsive" id="${image.id }" src="${image.link }">
    </div>

</div>
<form>
    <div class="col-md-4 portfolio-item" style="margin-left: 20px;">
        <div class="row">
            <br>
            <br>
            <h4><strong>Choose the tags according to the image shown:</strong></h4>
        </div>

        <div class="row">
            <c:forEach items="${image.tagsSelected}" var="tag">
                <%-- <span>${tag.tag.name}</span> --%>
                <input type="checkbox" value="${tag.tag.name}" checked>  ${tag.tag.name}<br/>
            </c:forEach>
        </div>
        <div class="row" style="padding-top: 30px;">
            <h5>Add your own tags:</h5>
            <input id="ownTags" type="text" placeholder="Add your own tags"/>
        </div>

        <div class="row" style="padding-top: 30px;">
            <h5>Add sentiments for this image:</h5>
            <form>
                <input type="radio" name="likes" id="sentiment1" class="likes" id-value="1">Strongly like<br>
                <input type="radio" name="likes" id="sentiment2" class="likes" id-value="2">Like more than medium </br>
                <input type="radio" name="likes" id="sentiment3" class="likes" id-value="3">Like medium </br>
                <input type="radio" name="likes" id="sentiment4" class="likes" id-value="4">Almost like </br>
                <input type="radio" name="likes" id="sentiment5" class="likes" id-value="5">Don't like it all </br>
            </form>
        </div>

        <div class="row" style="padding-top: 30px;">
            <button id="id1" name="id1" id-image="${image.id}" type="button" class="btn btn-default">Submit your changes</button>
        </div>

    </div>
</form>
<script>
    $('#ownTags').tagsInput();

    $("#id1").on('click',function () {
        var id = $(this).attr('id-image');
        console.log(id);
        var selectedTags= document.getElementById("ownTags").value;
        var like;
        like = $(".likes:checked").attr("id-value");
        if(like === undefined){
            like = 1; //default to mostly liked
        }

        $.ajax({
            type: 'POST',
            url: 'annotateImage',
            data: {
                imageId: id,
                selectedTags: selectedTags,
                like: like
            },
            success: function (data) {
                console.log("success");
                document.open();
                document.write(data);
                document.close();
            },
            error: function(data){
                console.log(data);
                document.open();
                document.write(data);
                document.close();
            }
        })
    });
</script>

