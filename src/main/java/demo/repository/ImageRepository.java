package demo.repository;

import demo.domain.Image;
import demo.domain.RealEstate;
import demo.domain.TagSelected;
import demo.domain.User;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
@Service
@Transactional
public class ImageRepository {
    @PersistenceContext
    private EntityManager em;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    UserRepository userRepository;

    public List<TagSelected> getListTagsForImage(long id) {
        Image image = em.find(Image.class, id);
        Hibernate.initialize(image);
        Hibernate.initialize(image.getTagsSelected());
        return image.getTagsSelected();
    }

    public Image getImageById(long id) {
        Image image = em.find(Image.class, id);
        return image;
    }

    public List<Image> getImages() {
        return em.createNamedQuery(Image.FIND_ALL).getResultList();
    }

    public void addLinksToImages(String links, String idImage) {
        String[] linksToken = links.split("@_@");
        Image im = em.find(Image.class, Long.parseLong(idImage));

        List<Image> images = new ArrayList<Image>();
        for (int i = 0; i < linksToken.length; i++) {
            String[] tks = linksToken[i].split(";;");
            Image image = new Image();
            image.setLink(tks[0]);
            image.setName(tks[1]);
            image.setKeyword("");
            image.setLocation("");
            em.persist(image);
            images.add(image);
        }
        em.flush();

        for (Image image : images) {
            persistTags(im.getTagsSelected(), image);
        }

    }

    private void persistTags(List<TagSelected> tags, Image image) {
        for (TagSelected tag : tags) {
            TagSelected newTag = new TagSelected();
            newTag.setImage(image);
            newTag.setPoints(0);
            newTag.setTag(tag.getTag());

            em.persist(newTag);
        }
    }

    public List<TagSelected> getTagsSelectedByImage(Long id) {
        Image im = em.find(Image.class, id);
        return im.getTagsSelected();
    }

    public List<TagSelected> getTagsForImage(List<Image> images) {
        List<List<TagSelected>> args = new ArrayList<List<TagSelected>>();
        for (Image image : images) {
            args.add(getTagsSelectedByImage(image.getId()));

        }
        return tagRepository.getCommonTags(args);
    }
    public List<Image> getImagesWithPagination(String page) {
        int pageNo = Integer.valueOf(page);
        return em.createNamedQuery(Image.FIND_ALL).setFirstResult(pageNo * 100).setMaxResults(100).getResultList();
    }
    public List<Image> getImagesWithPagination(String page, String userId) {
        int pageNo = Integer.valueOf(page);
        User user = userRepository.getUserByEmail(userId);
        return em.createNamedQuery(Image.FIND_ALL_NOT_ANNOTATED).setParameter("user", user).setFirstResult(pageNo * 100).setMaxResults(100).getResultList();
    }

    public void deleteImage(String imageId) {
        Image image = em.find(Image.class, Long.parseLong(imageId));
        em.remove(image);
    }

    public List<Image> ceva() {
        String userId = "14";
        User user = em.find(User.class, 14L);
        List<Image> resultList = em.createNamedQuery(Image.FIND_ALL_NOT_ANNOTATED).setParameter("user", user).setMaxResults(100).getResultList();
        return resultList;
    }

    public void annotateImage(String userId, String imageId) {
        User user = userRepository.getUserByEmail(userId);
        Image image = em.find(Image.class, Long.valueOf(imageId));

        List<User> users = image.getUsers();
        users.add(user);
        image.setUsers(users);
    }
    
    public List<RealEstate> getRealEstates() {
        return em.createNamedQuery(RealEstate.FIND_ALL).getResultList();
    }
}
