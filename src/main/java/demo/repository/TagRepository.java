package demo.repository;

import demo.domain.Tag;
import demo.domain.TagSelected;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
@Service
@Transactional
public class TagRepository {

	@PersistenceContext
	EntityManager em;

	public List<TagSelected> getCommonTags(List<List<TagSelected>> args) {
		int maxElements;
		List<TagSelected> tagsBase = args.get(0);
		List<TagSelected> tagsSelected = new ArrayList<TagSelected>();
	/*	maxElements = tagsBase.size();
		for (int i = 1; i < args.size(); i++) {
			if (maxElements < args.get(i).size()) {
				maxElements = args.get(i).size();
				tagsBase = args.get(i);
			}

		}
*/
		int appearance = 0;
		for (TagSelected tag : tagsBase) {
			for (int i = 0; i < args.size(); i++) {
				List<TagSelected> selected = args.get(i);
				if (isInCollection(tag, selected) == true) {
					appearance++;
				}
			}
			if (appearance >= 2) {
				tagsSelected.add(tag);
			}
			appearance = 0;
		}
		return tagsSelected;
	}

	private boolean isInCollection(TagSelected tag, List<TagSelected> selected) {
		for(TagSelected ttag : selected){
			if(ttag.getTag().getId() == tag.getTag().getId()){
				return true;
			}
		}
		return false;
	}

    public Tag getTagByName(String name){
        TypedQuery<Tag> query = (TypedQuery<Tag>) em.createNamedQuery(Tag.FIND_BY_NAME);
        query.setParameter("name", name);
        Tag tag;
        try{
            tag = query.getSingleResult();
            return tag;
        }catch(NoResultException e){
            return null;
        }
    }

    public Tag createTag(String name){
        Tag tag = new Tag();
        tag.setName(name);
        em.persist(tag);
		return tag;
    }
}
