package demo.repository;

import demo.domain.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@SuppressWarnings("unchecked")
@Service
@Transactional
public class UserRepository {

    @PersistenceContext
    private EntityManager em;

    public User getUserByEmail(String email) {

            TypedQuery<User> query = (TypedQuery<User>) em.createNamedQuery(User.FIND_BY_EMAIL);
            query.setParameter("email", email);
            User user;
            try{
                user = query.getSingleResult();
                return user;
            }catch(NoResultException e){
                return null;
            }
        }

    public void addUserInDb(String email) {
        if(getUserByEmail(email) == null){
            //add user in DB
            User user = new User();
            user.setEmail(email);
            user.setNrImages(0);
            em.persist(user);
        }
    }
}
