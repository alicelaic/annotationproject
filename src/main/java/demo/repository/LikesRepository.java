package demo.repository;

import demo.domain.Image;
import demo.domain.Like;
import demo.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Alice on 1/2/2015.
 */
@Service
@Transactional
public class LikesRepository {
    @Autowired
    UserRepository userRepository;

    @PersistenceContext
    private EntityManager em;

    public void addLikeToPicture(String value, String userEmail, String imageId){
        Like like = new Like();
        User user = userRepository.getUserByEmail(userEmail);
        Image image = em.find(Image.class, Long.valueOf(imageId));
        like.setImage(image);
        like.setUser(user);
        like.setValue(Integer.valueOf(value));
        em.persist(like);
    }
}
