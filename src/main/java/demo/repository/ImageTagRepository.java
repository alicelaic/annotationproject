package demo.repository;

import demo.domain.Image;
import demo.domain.ImageTag;
import demo.domain.Tag;
import demo.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@SuppressWarnings("unchecked")
@Service
@Transactional
public class ImageTagRepository {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ImageRepository imageRepository;

    public void persistUserTag(Tag tag, String imageId, String userEmail)
    {
        Image image=imageRepository.getImageById(Long.parseLong(imageId));
        User user=userRepository.getUserByEmail(userEmail);
        persistUserTag(tag,image,user);
    }

    public boolean isImageAnnotated(String imageId, String userEmail) {

        User user=userRepository.getUserByEmail(userEmail);
        TypedQuery<ImageTag> query = (TypedQuery<ImageTag>) em.createNamedQuery(ImageTag.IS_IMAGE_ANNOTATED_BY_USER);
        query.setParameter("imageId", imageId);
       // query.setParameter("userId", String.valueOf(user.getId()));

        try{
            ImageTag t=query.getSingleResult();
            return true;
        }catch(NoResultException e){
            return false;
        }

    }

    private void persistUserTag(Tag tag, Image image, User user)
    {
        ImageTag newTag = new ImageTag();
        newTag.setImage(image);
        newTag.setTag(tag);
        newTag.setUser(user);
        newTag.setIsVerified(0);

        em.persist(newTag);
    }



}
