package demo.domain;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "tags")
@NamedQueries({
        @NamedQuery(name = Tag.FIND_BY_NAME, query = "from Tag u WHERE u.name = :name")
})
public class Tag implements Serializable{
	public static final String FIND_BY_NAME = "Tag.findByName";
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="name")
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
