package demo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;


@SuppressWarnings("serial")
@NamedQueries({
    @NamedQuery(name = Image.FIND_ALL, query = "from Image"),

        @NamedQuery(name = Image.FIND_ALL_NOT_ANNOTATED, query = "SELECT u from Image u WHERE :user NOT MEMBER OF u.users"),

    @NamedQuery(name = Image.FIND_IMAGE_BY_URL, query = "from Image u WHERE u.link = :url")
})
@Entity
@Table(name = "images")
public class Image implements Serializable {
	public static final String FIND_ALL = "Image.findAll";
	public static final String FIND_IMAGE_BY_URL = "Image.findImageByUrl";
    public static final String FIND_ALL_NOT_ANNOTATED = "Image.findAllNotAnnotated";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="link")
	private String link;
	
	@Column(name="location")
	private String location;
	
	@Column(name="keyword")
	private String keyword;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}



	@OneToMany(cascade=CascadeType.ALL, mappedBy="image", fetch = FetchType.EAGER)
	private List<TagSelected> tagsSelected = Collections.emptyList();


	public List<TagSelected> getTagsSelected() {
		return tagsSelected;
	}

	public void setTagsSelected(List<TagSelected> tagsSelected) {
		this.tagsSelected = tagsSelected;
	}

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }


    @OneToMany
    @JoinTable(name = "user_image",joinColumns = @JoinColumn(name = "image_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    @ElementCollection(fetch = FetchType.EAGER)
    private List<User> users = Collections.emptyList();


}
