package demo.domain;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(name = User.FIND_BY_EMAIL, query = "from User u WHERE u.email = :email")
})

public class User implements Serializable {

    public static final String FIND_BY_EMAIL = "Image.findUserByEmail";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="email")
    private String email;

    @Column(name="nrImages")
    private Integer nrImages;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getNrImages() {
        return nrImages;
    }

    public void setNrImages(Integer nrImages) {
        this.nrImages = nrImages;
    }
}
