package demo.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="tags_selected")
@NamedQueries({
        @NamedQuery(name = TagSelected.FIND_BY_NAME, query ="select u from TagSelected u WHERE u.tag.name = :name")
})
public class TagSelected implements Serializable{

    public static final String FIND_BY_NAME = "TagSelected.findByName";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@JoinColumn(name="idImage")
	@ManyToOne
	private Image image;
	
	@JoinColumn(name="idTag")
	@ManyToOne
	private Tag tag;

	@Column(name="pointsPerTag")
	private Integer points;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Tag getTag() {
		return tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}
}
