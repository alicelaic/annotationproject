package demo.domain;

/**
 * Created by Alice on 12/3/2014.
 */
public class UserAuthenticate {
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String username;
    private String password;

}
