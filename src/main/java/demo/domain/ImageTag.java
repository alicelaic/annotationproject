package demo.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tags_images")
@NamedQueries({
        @NamedQuery(name = ImageTag.IS_IMAGE_ANNOTATED_BY_USER, query = "select u from ImageTag u WHERE u.image.id= :imageId")
})

public class ImageTag implements Serializable {

    public static final String IS_IMAGE_ANNOTATED_BY_USER = "ImageTag.isImageAnnotatedByUser";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "idImage")
    @ManyToOne
    private Image image;

    @JoinColumn(name = "idTag")
    @ManyToOne
    private Tag tag;

    @Column(name = "isVerified")
    private Integer isVerified;

    @JoinColumn(name = "idUser")
    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Integer getIsVerified() { return isVerified; }

    public void setIsVerified(Integer isVerified) {this.isVerified = isVerified;}

    public Tag getTag() { return tag; }

    public void setTag(Tag tag) { this.tag = tag; }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }
}
