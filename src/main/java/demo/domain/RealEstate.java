package demo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;


@SuppressWarnings("serial")
@NamedQueries({
    @NamedQuery(name = RealEstate.FIND_ALL, query = "from RealEstate"),
})
@Entity
@Table(name = "real_estate")
public class RealEstate implements Serializable {
	public static final String FIND_ALL = "RealEstate.findAll";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="title")
	private String title;
	
	@Column(name="description")
	private String description;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
