package demo.controller;

import demo.domain.Tag;
import demo.repository.ImageRepository;
import demo.repository.ImageTagRepository;
import demo.repository.LikesRepository;
import demo.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@Scope("session")
@RequestMapping(value = "/realestate")
public class ImageController {

	@Autowired
	ImageRepository imageRepository;

	@Autowired
	TagRepository tagRepository;

	@Autowired
	ImageTagRepository imageTagRepository;

    @Autowired
    LikesRepository likesRepository;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(Model model) {

		//model.addAttribute("images", imageRepository.getImages());
		model.addAttribute("images", imageRepository.getRealEstates());
		return "account/view";
	}

    @RequestMapping(value = "/{page}", method = RequestMethod.GET)
    public String showAllWithPagination(@PathVariable(value="page") final String page, Model model, HttpServletRequest request) {
        request.getSession().setAttribute("pageNo", page);
        model.addAttribute("images", imageRepository.getImagesWithPagination(page, request.getUserPrincipal().getName()));
		return "account/annotation";
       // return "account/view";
    }

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	public String showNextImages(Model model,HttpServletRequest request) {
        Integer maxPage = 134;
		String pageNo=(String)request.getSession().getAttribute("pageNo");
		int newPageNo=Integer.parseInt(pageNo)+1;
        if(newPageNo <= maxPage) {
            System.out.println(newPageNo + ".." + String.valueOf(newPageNo));
            request.getSession().setAttribute("pageNo", String.valueOf(newPageNo));
            model.addAttribute("images", imageRepository.getImagesWithPagination(String.valueOf(newPageNo), request.getUserPrincipal().getName()));
        }else{
            request.getSession().setAttribute("pageNo", "134");
            model.addAttribute("images", imageRepository.getImagesWithPagination("134", request.getUserPrincipal().getName()));
        }
		return "account/annotation";
		//return "account/view";
	}

	@RequestMapping(value = "/isAnnotated", method = RequestMethod.POST)
	public boolean isImageAnnotated(@RequestParam("imageId") String imageId,HttpServletRequest request) {

		Principal p = request.getUserPrincipal();
		String userEmail=p.getName();
		boolean isAnnotated=imageTagRepository.isImageAnnotated(imageId,userEmail);
		return true;
	}

	@RequestMapping(value = "/previous", method = RequestMethod.GET)
	public String showPreviousImages(Model model,HttpServletRequest request) {

		String pageNo=(String)request.getSession().getAttribute("pageNo");
		int newPageNo=Integer.parseInt(pageNo)-1;
        if(newPageNo < 0){
            request.getSession().setAttribute("pageNo","0");
            model.addAttribute("images", imageRepository.getImagesWithPagination("0", request.getUserPrincipal().getName()));

        }else {
            request.getSession().setAttribute("pageNo", String.valueOf(newPageNo));
            model.addAttribute("images", imageRepository.getImagesWithPagination(String.valueOf(newPageNo), request.getUserPrincipal().getName()));
        }
		return "account/annotation";
		//return "account/view";
	}

	/*@RequestMapping(value = "/links", method = RequestMethod.POST)
	public String addLinksToDb(HttpServletRequest request, Model model) {
		String links = request.getParameter("links");
		String idImage = request.getParameter("idImage");
		imageRepository.addLinksToImages(links, idImage);
		return "account/view";
	}
*/
    @RequestMapping(value = "/id={id}", method = RequestMethod.GET)
    public String showImage(@PathVariable(value="id") final Long id, Model model){

       // model.addAttribute("image", imageRepository.getImageById(id));
       // model.addAttribute("tags", imageRepository.getTagsSelectedByImage(id));
        return "account/viewTags";
    }

		@RequestMapping(value = "/downloadAllImages", method = RequestMethod.GET)
		public String downloadImages(HttpServletRequest request, Model model) {
			//imageRepository.downloadImages();
			return "account/view";
		}

		@RequestMapping(value = "/deleteImage", method = RequestMethod.POST)
		@ResponseBody
		public String deleteImage(@RequestParam("imageId") String imageId, HttpServletRequest request, Model model) {
			imageRepository.deleteImage(imageId);
			return "removed";
    }

	@RequestMapping(value = "/annotateImage", method = RequestMethod.POST)
	public String annotateImage(@RequestParam("imageId") String imageId, @RequestParam("selectedTags")  String selectedTags, @RequestParam("like") String like,  HttpServletRequest request, Model model) {

		Principal p = request.getUserPrincipal();
		String userEmail=p.getName();

		String[] userTags=selectedTags.split(",");
        if(userTags.length > 0) {
            for (String userTag : userTags) {
                System.out.println(userTag);
                Tag tag = tagRepository.getTagByName(userTag);
                if (tag == null) {
                    tag = tagRepository.createTag(userTag);
                }
                imageTagRepository.persistUserTag(tag, imageId, userEmail);
            }

            likesRepository.addLikeToPicture(like, userEmail, imageId);
            //add image to user meaning that he annotated
            imageRepository.annotateImage(userEmail, imageId);
        }
        model.addAttribute("images", imageRepository.getImagesWithPagination((String)request.getSession().getAttribute("pageNo"), request.getUserPrincipal().getName()));
		return "account/annotation";
	}

}
