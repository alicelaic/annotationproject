package demo.controller;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import demo.domain.ServerAuthenticationException;
import demo.domain.UserAuthenticate;
import demo.repository.ImageRepository;
import demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Alice on 12/3/2014.
 */
@Controller
public class HomeController {

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    UserRepository userRepository;
    
    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(){
       return "account/login";
    }


    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String login(Model model){
       //return "account/login";
    	model.addAttribute("images", imageRepository.getRealEstates());
		return "account/view";
    }

    @RequestMapping(value = "/doLogin", method = RequestMethod.POST)
    public String authenticate(HttpServletRequest request,Model model) throws IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        //uncomment this if you dont want to login with your fenrir credentials

       /* if(username.length() > 0 && password.length() > 0){
            authenticateWithSpring(username, password);
            request.getSession().setAttribute("pageNo","0");
            userRepository.addUserInDb(username);
            model.addAttribute("images", imageRepository.getImagesWithPagination("0"));
            return "account/annotation";

        }*/
        try {
                authenticateToExternalServer(username, password, "fenrir.info.uaic.ro", 22);
                userRepository.addUserInDb(username);
                 request.getSession().setAttribute("pageNo","0");
                model.addAttribute("images", imageRepository.getImagesWithPagination("0", username));
        } catch (ServerAuthenticationException sae) {
            return "error";
        }
        return "account/annotation";
       // return "account/view";
    }

    private void authenticateToExternalServer(String username, String password, String host, int port) throws ServerAuthenticationException {
        Session session = tryCreatingSession(username, host, port, new JSch());
        setSessionConfig(session);
        session.setPassword(password);
        tryConnecting(session);
        authenticateWithSpring(username, password);
        session.disconnect();
    }

    private void authenticateWithSpring(String username, String password) {
        UserAuthenticate springUser = createUser(username, password);
        List<GrantedAuthority> auth = new ArrayList<GrantedAuthority>();
        auth.add(new SimpleGrantedAuthority("ROLE_REG"));
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                springUser.getUsername(), springUser.getPassword(), auth);
        SecurityContextHolder.getContext().setAuthentication(authentication);

    }

    private Session tryCreatingSession(String username, String host, int port, JSch jsch) throws ServerAuthenticationException {
        try {
            return jsch.getSession(username, host, port);
        } catch (JSchException e) {
            throw new ServerAuthenticationException("Failed to establish connection to " + host);
        }
    }

    private void setSessionConfig(Session session) {
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
    }

    private void tryConnecting(Session session) throws ServerAuthenticationException {
        try {
            session.connect();
        } catch (JSchException e) {
            throw new ServerAuthenticationException("Invalid username or password!");
        }
        if (!session.isConnected()) {
            throw new ServerAuthenticationException("There was a problem when trying to connect to " + session.getHost());
        }
    }

    private UserAuthenticate createUser(String username, String password){
        UserAuthenticate userAuthenticate = new UserAuthenticate();
        userAuthenticate.setPassword(password);
        userAuthenticate.setUsername(username);
        return userAuthenticate;
    }
}
