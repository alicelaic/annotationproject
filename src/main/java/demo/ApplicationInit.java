package demo;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Alice on 11/23/2014.
 */
public class ApplicationInit implements ApplicationContextInitializer {
    public static Map<String, String> PROPERTIES = new HashMap<String, String>();


    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        readProperties();
    }

    private void readProperties() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            String filename = "application.properties";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            if (input == null) {
                System.out.println("File not found: " + filename);
            }
            prop.load(input);
            Enumeration<?> e = prop.propertyNames();
            while (e.hasMoreElements()) {
                String key = (String) e.nextElement();
                String value = prop.getProperty(key);
                PROPERTIES.put(key, value);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
